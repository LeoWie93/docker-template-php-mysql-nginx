# PHP 7.4 Docker Template

## `docker-compose.yml`
Defines all containers:
- MYSQL
    - default version is 8.0.22
    - environment variables for DB settigns
    - host:conainter ports default to `9002:3306`
- webserver
    - host:conainter ports default to `9000:80`
- php-fpm

## `_docker/php-fpm/Dockerfile`
Defines the php docker container and installs custom packages and sets priviliges while bulding the container

## `_docker/nginx/nginx.conf`
Defines custom settings for the webserver.
Important are:
- index
    - path to your index file from `./` of your project
- server_name
    - the server name that you will set inside your `hosts` file